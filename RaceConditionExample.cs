﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;


namespace Parallel
{
    public class RaceConditionExample
    {
        private class StateObject
        {
            private int state = 5;
            public void ChangeState(int loop)
            {
                if (state == 5)
                {
                    state++;
                    Trace.Assert(state == 6, "Race condition appears after " + loop + " cycles");
                }
                state = 5;
            }
        }

        private class SampleThread
        {
            public void RaceCondition(object obj)
            {
                Trace.Assert(obj is StateObject, "obj must be StateObject type");
                var state = obj as StateObject;
                int i = 0;
                while (true)
                    state.ChangeState(i++);
            }
        }

        public static void Execute()
        {
            var state = new StateObject();
            for (var i = 0; i < 20; i++)
                new Task(new SampleThread().RaceCondition, state).Start();
            Thread.Sleep(1000);
        }
    }
}
