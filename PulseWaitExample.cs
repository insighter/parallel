﻿using System;
using System.Threading;

namespace Parallel
{
    public class PulseWaitExample
    {
        private class TickTock
        {
            private object lockOn = new object();

            public void Tick(bool running)
            {
                lock (lockOn)
                {
                    if (!running)
                    {                        
                        Monitor.Pulse(lockOn);
                        return;
                    }

                    Console.Write("tick ");
                    
                    Monitor.Pulse(lockOn);
                   
                    Monitor.Wait(lockOn);
                }
            }

            public void Tock(bool running)
            {
                lock (lockOn)
                {
                    if (!running)
                    {
                        Monitor.Pulse(lockOn);
                        return;
                    }

                    Console.WriteLine("tock ");
                    Monitor.Pulse(lockOn);
                    Monitor.Wait(lockOn);
                }
            }
        }

        private class MyThread
        {
            public Thread thrd;
            TickTock ttobj;
            
            public MyThread(string name, TickTock tt)
            {
                thrd = new Thread(this.Run);
                ttobj = tt;
                thrd.Name = name;
                thrd.Start();
            }

            void Run()
            {
                if (thrd.Name == "Tick")
                {
                    for (int i = 0; i < 5; i++)
                        ttobj.Tick(true);
                    ttobj.Tick(false);
                }
                else
                {
                    for (int i = 0; i < 5; i++)
                        ttobj.Tock(true);
                    ttobj.Tock(false);
                }
            }
        }
     
        public static void Execute()
        {
            TickTock tt = new TickTock();
            MyThread mt1 = new MyThread("Tick", tt);
            MyThread mt2 = new MyThread("Tock", tt);
            mt1.thrd.Join();
            mt2.thrd.Join();

            Console.WriteLine("Watch stopped");
            Console.ReadLine();
        }        
    }
}
