﻿using System;
using System.Threading;

namespace Parallel
{
    public class ThreadSyncProblem
    {
        private class MyThread
        {
            public void ThreadNumbers()
            {                
                Console.WriteLine("{0}; thread uses method ThreadNumbers", Thread.CurrentThread.Name);
             
                Console.Write("Numbers: ");
                for (int i = 0; i < 10; i++)
                {
                    var rand = new Random();
                    Thread.Sleep(1000 * rand.Next(5));
                    Console.Write(i + ", ");
                }
                Console.WriteLine();
            }
        }

        public static void Execute()
        {
            var mt = new MyThread();
            
            var threads = new Thread[10];

            for (int i = 0; i < 10; i++)
            {
                threads[i] = new Thread(new ThreadStart(mt.ThreadNumbers));
                threads[i].Name = $"Thread {i} works";
            }

            foreach (var t in threads)
                t.Start();

            Console.ReadLine();
        }
    }
}
