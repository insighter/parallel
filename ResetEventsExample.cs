﻿using System;
using System.Threading;

namespace Parallel
{
    public class ResetEventsExample
    {
        private class MyThread
        {
            private Thread thrd;
            private EventWaitHandle mre;

            public MyThread(string name, EventWaitHandle evt)
            {
                thrd = new Thread(this.Run);
                thrd.Name = name;
                mre = evt;
                thrd.Start();
            }

            void Run()
            {
                Console.WriteLine("Inside thread " + thrd.Name);

                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine(thrd.Name);
                    Thread.Sleep(500);
                }

                Console.WriteLine(thrd.Name + " completed!");
                
                mre.Set();
            }
        }

        public static void Execute()
        {
            ExecuteManualResetEventExample();
            ExecuteAutoResetEventExample();
        }

        private static void ExecuteManualResetEventExample()
        {
            var evtObj = new ManualResetEvent(false);

            var mt1 = new MyThread("Thread 1", evtObj);

            Console.WriteLine("Main thread is waiting manual reset event");

            evtObj.WaitOne();

            Console.WriteLine("Main thread got notification about manual reset event from thread 1");

            evtObj.Reset();

            mt1 = new MyThread("Thread 2", evtObj);

            evtObj.WaitOne();

            Console.WriteLine("Main thread got notification about manual reset event from thread 2");
            Console.ReadLine();
        }

        private static void ExecuteAutoResetEventExample()
        {
            var evtObj = new AutoResetEvent(false);

            var mt1 = new MyThread("Thread 1", evtObj);

            Console.WriteLine("Main thread is waiting auto reset event");

            evtObj.WaitOne();

            Console.WriteLine("Main thread got notification about auto reset event from thread 1");            

            mt1 = new MyThread("Thread 2", evtObj);

            evtObj.WaitOne();

            Console.WriteLine("Main thread got notification about auto reset event from thread 2");
            Console.ReadLine();
        }
    }
}
