﻿using System;
using System.Threading;

namespace Parallel
{
    public class TimerExample
    {
        private static void PrintTime(object state)
        {
            Console.Clear();
            Console.WriteLine($"Current time:  {DateTime.Now.ToLongTimeString()}");
        }

        public static void Execute()
        {            
            var timeCB = new TimerCallback(PrintTime);

            var time = new Timer(timeCB, null, 0, 1000);
            Console.WriteLine("Press to quit");
            Console.ReadLine();
        }
    }
}
