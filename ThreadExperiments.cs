﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Parallel
{ 
    class ThreadExperiments
    {   
        static void Main(string[] args)
        {
            //ThreadStartExample.Execute();
            //ParametrizedThreadStartExample.Execute();
            //AutoResetEventExample.Execute();
            //RaceConditionExample.Execute();
            //ThreadSyncProblem.Execute();
            //ThreadSyncExample.Execute();
            //PulseWaitExample.Execute();
            //MutexExample.Execute();
            //ResetEventsExample.Execute();
            //TimerExample.Execute();
            //ThreadPoolExample.Execute();
            //ReadWriteLockSlimExample.Execute();
            ReadWriteLockSlimExample2.Execute();
        }
    }
}
