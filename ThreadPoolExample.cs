﻿using System;
using System.Threading;

namespace Parallel
{
    public class ThreadPoolExample
    {
        public static void Execute()
        {
            int nWorkerThreads;
            int nCompletionThreads;
            ThreadPool.GetMaxThreads(out nWorkerThreads, out nCompletionThreads);
            Console.WriteLine($"Max thread count: {nWorkerThreads}\nCompletion threads are available: {nCompletionThreads}");
            for (int i = 0; i < 5; i++)
                ThreadPool.QueueUserWorkItem(JobForAThread);
            Thread.Sleep(3000);

            Console.ReadLine();
        }

        private static void JobForAThread(object state)
        {
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine($"cycle {i}, execution inside thread from pool {Thread.CurrentThread.ManagedThreadId}");
                Thread.Sleep(50);
            }
        }   
    }
}
