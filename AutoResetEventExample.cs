﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Parallel
{
    public class AutoResetEventExample
    {        
        public static void Execute()
        {
            Console.WriteLine("Main thread. ID: " + Thread.CurrentThread.ManagedThreadId);

            var waitHandle = new AutoResetEvent(false);

            var pm = new ParamsForAutoResetEventExample(10, 10, waitHandle);
            var t = new Thread(new ParameterizedThreadStart(Add));
            t.Start(pm);
            
            waitHandle.WaitOne();
            Console.WriteLine("All thread completed");

            Console.ReadLine();
        }

        static void Add(object obj)
        {
            if (obj is ParamsForAutoResetEventExample)
            {
                Console.WriteLine("Thread ID of method Add(): " + Thread.CurrentThread.ManagedThreadId);
                var pr = (ParamsForAutoResetEventExample)obj;
                Console.WriteLine("{0} + {1} = {2}", pr.A, pr.B, pr.A + pr.B);
                
                pr.WaitHandle.Set();
            }
        }

        private class ParamsForAutoResetEventExample
        {
            public int A { get; }

            public int B { get; }

            public AutoResetEvent WaitHandle { get; }

            public ParamsForAutoResetEventExample(int a, int b, AutoResetEvent waitHandle)
            {
                A = a;
                B = b;
                WaitHandle = waitHandle;
            }
        }
    }
}
