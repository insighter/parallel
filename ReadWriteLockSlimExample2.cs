﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Parallel
{
    public class ReadWriteLockSlimExample2
    {
        public static void Execute()
        {
            var rwLock = new ReaderWriterLockSlim();

            var lockObj = new ManualResetEvent(false);

            var readWaitHandle1 = new ManualResetEvent(false);
            var paramForReader = new ParamForReadWriteLockSlim(rwLock, "reader 1", lockObj, new[] { readWaitHandle1 });
            var thread1 = new Thread(new ParameterizedThreadStart(GetReadLock));
            thread1.Start(paramForReader);

            //Thread.Sleep(1000);

            var readWaitHandle2 = new ManualResetEvent(false);
            var paramForReader2 = new ParamForReadWriteLockSlim(rwLock, "reader 2", lockObj, new[] { readWaitHandle2 });
            var thread2 = new Thread(new ParameterizedThreadStart(GetReadLock));
            thread2.Start(paramForReader2);

            //Thread.Sleep(1000);

            var paramForUpgradableReader = new ParamForReadWriteLockSlim(rwLock, "upgradable reader", lockObj, new ManualResetEvent[] { });
            var thread3 = new Thread(new ParameterizedThreadStart(GetUpgradableReadLock));
            thread3.Start(paramForUpgradableReader);

            Thread.Sleep(1000);

            var paramForWriter = new ParamForReadWriteLockSlim(rwLock, "writer", lockObj, new[] { readWaitHandle1, readWaitHandle2 });
            var thread4 = new Thread(new ParameterizedThreadStart(GetWriterLock));
            thread4.Start(paramForWriter);

            Console.ReadLine();
        }

        static void GetReadLock(object obj)
        {
            if (obj is ParamForReadWriteLockSlim)
            {
                var param = (ParamForReadWriteLockSlim)obj;
                var gotLock = param.RwLock.TryEnterReadLock(0);
                var verb = gotLock ? "got" : "failed to get";
                Console.WriteLine($"{param.WorkerName} {verb} the read lock");
                param.LockObj.WaitOne();
                if (gotLock)
                {
                    param.RwLock.ExitReadLock();
                    Console.WriteLine($"{param.WorkerName} exited the read lock");
                }
                param.WaitHandles[0].Set();
            }
        }

        static void GetUpgradableReadLock(object obj)
        {
            if (obj is ParamForReadWriteLockSlim)
            {
                var param = (ParamForReadWriteLockSlim)obj;
                var gotLock = param.RwLock.TryEnterUpgradeableReadLock(0);
                var verb = gotLock ? "got" : "failed to get";
                Console.WriteLine($"{param.WorkerName} {verb} the upgradable read lock");
                if (gotLock)
                {
                    try
                    {
                        //var gotWriteLock = param.RwLock.TryEnterWriteLock(0);
                        //var verb2 = gotWriteLock ? "got" : "failed to get";
                        //Console.WriteLine($"{param.WorkerName} {verb2} the write lock");
                        //if (gotWriteLock)
                        //{
                        //    param.RwLock.ExitWriteLock();
                        //    Console.WriteLine($"{param.WorkerName} exited the write lock");
                        //}
                    }
                    finally
                    {
                        param.RwLock.ExitUpgradeableReadLock();
                        Console.WriteLine($"{param.WorkerName} exited the upgradable read lock");
                    }
                }
            }
        }

        static void GetWriterLock(object obj)
        {
            if (obj is ParamForReadWriteLockSlim)
            {
                var param = (ParamForReadWriteLockSlim)obj;              
                param.LockObj.Set();
                WaitHandle.WaitAll(param.WaitHandles);
                var gotLock = param.RwLock.TryEnterWriteLock(0);
                var verb = gotLock ? "got" : "failed to get";
                Console.WriteLine($"{param.WorkerName} {verb} the write lock");
                if (gotLock)
                {
                    param.RwLock.ExitWriteLock();
                    Console.WriteLine($"{param.WorkerName} exited the write lock");
                }
            }
        }

        private class ParamForReadWriteLockSlim
        {
            public ParamForReadWriteLockSlim(ReaderWriterLockSlim rwLock, string workerName, ManualResetEvent lockObj, ManualResetEvent[] waitHandles)
            {
                RwLock = rwLock;
                WorkerName = workerName;
                LockObj = lockObj;
                WaitHandles = waitHandles;
            }

            public ReaderWriterLockSlim RwLock { get; }

            public string WorkerName { get; }

            public ManualResetEvent LockObj { get; }

            public ManualResetEvent[] WaitHandles { get; }
        }
    }
}
