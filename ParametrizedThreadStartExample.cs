﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading; 

namespace Parallel
{ 
    public class ParametrizedThreadStartExample
    {
        static void Add(object obj)
        {
            if (obj is Params)
            {
                Console.WriteLine("Thread ID of method Add(): " + Thread.CurrentThread.ManagedThreadId);
                var pr = (Params)obj;
                Console.WriteLine("{pr.A} + {pr.B} = {pr.A + pr.B}");
            }
        }

        public static void Execute()
        {
            Console.WriteLine($"Main thread. ID: {Thread.CurrentThread.ManagedThreadId}");

            var pm = new Params(10, 10);
            var t = new Thread(new ParameterizedThreadStart(Add));
            t.Start(pm);
            
            Thread.Sleep(5);
            Console.ReadLine();
        }

        private class Params
        {
            public int A { get; }

            public int B { get; }

            public Params(int a, int b)
            {
                A = a;
                B = b;
            }
        }
    }
}
