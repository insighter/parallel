﻿using System;
using System.Threading;

namespace Parallel
{
    public class MutexExample
    {
        class SharedRes
        {
            public static int Count;
            public static Mutex Mtx = new Mutex();
        }
        
        class IncThread
        {
            int num;
            public Thread Thrd;

            public IncThread(string name, int n)
            {
                Thrd = new Thread(this.Run);
                num = n;
                Thrd.Name = name;
                Thrd.Start();
            }
            
            void Run()
            {
                Console.WriteLine(Thrd.Name + " is waiting mutex");
                
                SharedRes.Mtx.WaitOne();

                Console.WriteLine(Thrd.Name + " is getting mutex");

                do
                {
                    Thread.Sleep(500);
                    SharedRes.Count++;
                    Console.WriteLine("in thread {0}, Count={1}", Thrd.Name, SharedRes.Count);
                    num--;
                } while (num > 0);

                Console.WriteLine(Thrd.Name + " is making mutex free");

                SharedRes.Mtx.ReleaseMutex();
            }
        }

        class DecThread
        {
            int num;
            public Thread Thrd;

            public DecThread(string name, int n)
            {
                Thrd = new Thread(new ThreadStart(this.Run));
                num = n;
                Thrd.Name = name;
                Thrd.Start();
            }
            
            void Run()
            {
                Console.WriteLine(Thrd.Name + " is waiting mutex");
                
                SharedRes.Mtx.WaitOne();

                Console.WriteLine(Thrd.Name + " is getting mutex");

                do
                {
                    Thread.Sleep(500);
                    SharedRes.Count--;
                    Console.WriteLine("in thread {0}, Count={1}", Thrd.Name, SharedRes.Count);
                    num--;
                } while (num > 0);

                Console.WriteLine(Thrd.Name + " is making mutex free");

                SharedRes.Mtx.ReleaseMutex();
            }
        }
      
        public static void Execute()
        {
            var mt1 = new IncThread("Inc thread", 5);

            Thread.Sleep(1);

            var mt2 = new DecThread("Dec thread", 5);

            mt1.Thrd.Join();
            mt2.Thrd.Join();

            Console.ReadLine();
        }        
    }
}
