﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Parallel
{       
    public class ThreadStartExample
    {
        private void ThreadNumbers()
        {            
            Console.WriteLine("{0} thread uses method ThreadNumbers", Thread.CurrentThread.Name);
         
            Console.Write("Numbers: ");
            for (var i = 0; i < 10; i++)
            {
                Console.Write(i + ", ");         
                Thread.Sleep(3000);
            }
            Console.WriteLine();
        }

        public static void Execute()
        {
            Console.Write("How many threads use (1 or 2)?");
            var number = Console.ReadLine();

            var mythread = Thread.CurrentThread;
            mythread.Name = "First";
            
            Console.WriteLine("--> {0} main thread", Thread.CurrentThread.Name);
            var mt = new ThreadStartExample();

            switch (number)
            {
                case "1":
                    mt.ThreadNumbers();
                    break;
                case "2":                    
                    var backgroundThread = new Thread(new ThreadStart(mt.ThreadNumbers));
                    backgroundThread.Name = "Second";
                    backgroundThread.Start();
                    break;
                default:
                    Console.WriteLine("use 1 thread");
                    goto case "1";
            }
            MessageBox.Show("Message ...", "Executing other thread");
            Console.ReadLine();
        }
    }
}
